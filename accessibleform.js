jQuery(document).ready(function($) {
    window.brad = b = new Object;
    window.formCallbacks = b.formCallbacks = fc = new Object;
    window.customElement = b.customElement = new Object;
    
    b.makeuid = function() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    
        for( var i=0; i < 5; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));
    
        return text;
    }
      
    $.fn.validateForm = function() {
        var form = $( this );
        if( form.get(0).tagName === 'FORM' ) {
            
            var inputs = form.find( 'input[type="text"], input[type="email"], input[type="password"], input[type="file"], textarea, select, input[type="radio"], input[type="checkbox"]' );
            var pass = true;
            inputs.each( function() {
                var results = $( this ).validate();
                
                if( ( results.length > 0 ) || ( !results ) ) {
                    pass = false;
                    return true;
                }
            } );
            return pass;
        }
    }
    
    $.fn.validate = function() {
        var input = $( this );
        var id = input.attr( 'id' ) || input.attr( 'name' );
        

        var numericRegex = /^[0-9]+$/,
            integerRegex = /^\-?[0-9]+$/,
            decimalRegex = /^\-?[0-9]*\.?[0-9]+$/,
            emailRegex = /^[a-zA-Z0-9.!#$%&amp;'*+-\/=?\^_`{|}~\-]+@[a-zA-Z0-9\-]+(?:\.[a-zA-Z0-9\-]+)*$/,
            alphaRegex = /^[a-z]+$/i,
            internationalAlphaRegex = /^([ \u00c0-\u01ffa-zA-Z'\-])+$/,
            alphaNumericRegex = /^[a-z0-9]+$/i,
            alphaDashRegex = /^[a-z0-9_\-]+$/i,
            naturalRegex = /^[0-9]+$/i,
            naturalNoZeroRegex = /^[1-9][0-9]*$/i,
            ipRegex = /^((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){3}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})$/i,
            base64Regex = /[^a-zA-Z0-9\/\+=]/i,
            numericDashRegex = /^[\d\-\s]+$/,
            usernameRegex = /^[a-z0-9]+([a-z0-9](_|-)[a-z0-9])*[a-z0-9]+$/,
            phoneRegex = /^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/,
            //usernameRegex = /^[a-zA-Z0-9]+([a-zA-Z0-9](_|-)[a-zA-Z0-9])*[a-zA-Z0-9]+$/,
            whitespaceRegex = /^\s+$/,
            empty = /^$/;
        var value = input.val(),
            flags = [],
            validation_vocabulary = {};
            
        var placeholderValue = input.attr('placeholder');
        
        if( value.indexOf(placeholderValue) != -1 ) {
            value = '';
        }
            
        if (typeof input.data('validation') != "undefined") {
            var rules = (input.data('validation')).split(" ");
        } else {
            return true;
        }
        
        if ( input.parent().children('.error-message').length <= 0 ) {
            input.parent().append('<div class="error-message"></div>');
        }
        
        var error_message = input.parent().children('.error-message');
        $.each(rules, function() {
            rule = this;
            
            // REQUIRED
            if ( ( rule == "required" ) && ( ( empty.test( value ) ) || ( whitespaceRegex.test( value ) ) ) ) {
                flags.push( "required" );
                validation_vocabulary.required = "Ce champ est requis.";
            } else if( ( rule == "required" ) ) {
                var type = input.prop( 'type' );

                if( type === 'radio' ) {
                    var checked = input.is( ':checked' );
                    if( !checked ) {
                        checked = input.siblings().is( ':checked' );
                        
                        if ( checked ) {
                            return;
                        }
                    } else {
                        return;
                    }
                } else if( type === 'checkbox' ) {
                    var checked = input.is( ':checked' );
                    if( checked ) {
                        return;
                    }
                } else {
                    return true;
                }
                
                flags.push( "required" );
                validation_vocabulary.required = "Ce champ est requis.";
            }
            
            // CHAR
            
            if ( (/^char/.test(rule)) ) {
                var arguments = rule.split(":");
                
                if ( ((arguments.length == 3) && (arguments[2] == "not-required")) || ((arguments.length == 2)) ) {
                    if (arguments[1].indexOf("~") > 0) {
                        var char_nums = arguments[1].split("~");
                        var test = ((value.length >= char_nums[0]) && (value.length <= char_nums[1]));
                    } else {
                        var test = (value.length >= arguments[1]);
                    }
                    if (!(test)) {
                        flags.push("char");
                        validation_vocabulary.char = "Ce champ n'est pas de la bonne longueur.";
                    }
                }
                //flags.push("text");
            }
            
            if (!(empty.test(value))) {
            
                // NUMERIC
                
                if ( (rule == "numeric") && (!(numericRegex.test(value))) ) {
                    flags.push("numeric");
                    validation_vocabulary.numeric = "Ce champ ne peux contenir que des chiffres.";
                }
                
                // EMAIL
                
                if ( (rule == "email") && (!(emailRegex.test(value))) ) {
                    flags.push("email");
                    validation_vocabulary.email = "Ceci n'est pas un courriel valide.";
                }
                
                // PHONE
                
                if ( (rule == "phone") && (!(phoneRegex.test(value))) ) {
                    flags.push("phone");
                    validation_vocabulary.phone = "Ceci n'est pas un numéro de téléphone valide.";
                }
            
            }
            
            // MATCH
            
            if ( /^match/.test(rule) ) {
                var arguments = rule.split(":");
                
                if ( arguments.length == 2 ) {
                    var input_to_match = $('#' + arguments[1]);
                    var value_to_match = input_to_match.val();
                    
                    if ( value_to_match != value ) {
                        flags.push("match");
                        validation_vocabulary.match = "Ce champ est différent.";
                    }
                }
            }
        });

        if (flags.length > 0) {
            input.parents('.input').addClass('error');
            
            error_message.html('').text('');
            $.each(flags, function() {
                var flag = this;
                
                var validation_text = input.data('validation-text-' + flag) || validation_vocabulary[flag];
                
                error_message.append('<label for="' + id + '">' + validation_text + '</label><br/>');
            });
        } else {
            input.parents('.input').removeClass('error');
            var error_message = input.parent().children('.error-message');

            error_message.remove();
        }
        
        
        return flags
    };
    
    $.fn.getTemplate = function(templateName, attributes) {
        /*
try {
            var options = attributes.options;
        } catch( error ) {
            var options = new Object;
            options['template_' + templateName] = undefined;
        }
*/
        var jsOutput;
        try {
            var options = attributes.options;
            jsOutput = options['template_' + templateName];
        } catch( error ) {}
        
        var makeLoop = function(string, object) {
            var new_string = new String;
            for (var i = 0; i < object.length; i++) {
                new_string += factorize(string, object[i]);
            }
            return new_string;
        }
        var factorize = function(string, new_attributes) {
            attributes = new_attributes || attributes;
            try {
                var factory = new Object;
                var isloop = string.match(/(for)\s+\(([a-zA-Z]+)\s+(in)\s+([a-zA-Z]+)\)/);

                if (isloop) {
                    var loopDeclaration = isloop;

                    var forDeclaration = [string.indexOf('{{ ' + loopDeclaration[0] + ' }}'), ('{{ ' + loopDeclaration[0] + ' }}').length]; // Start of the for declaration and length of the declaration
                    var endForDeclaration = [string.indexOf('{{ endfor }}'), ('{{ endfor }}').length];
                    if (loopDeclaration[3] != 'in') {
                        // Just to make sure everything is at the right place
                        console.error("Forloop: The syntax seem wrong...");
                        return false;
                    }
                    var token = loopDeclaration[2];
                    var tokens = loopDeclaration[4];
                    var beforeLoop = string.slice(0, forDeclaration[0]);
                    var stringToFactorize = string.slice(forDeclaration[0] + forDeclaration[1], endForDeclaration[0]);
                    var afterLoop = string.slice(endForDeclaration[0] + endForDeclaration[1]);
                    var string = beforeLoop;

                    string += makeLoop(stringToFactorize, attributes[tokens]);
                    string += afterLoop;
                } else {
                    var blades = string.match(/({{)\s(([^}]*))\s(}})/g);
                    if (blades) {
                        var cleanedBlade = blades.map(function(b) {
                            return b.replace(/[}{]*/g, '').replace(/^\s\s*/, '').replace(/\s\s*$/, '');
                        });
                        
                        // For string and template
                        for (var i = 0; i < cleanedBlade.length; i++) {
                            var new_factory = new Object;
                            new_factory.clean = cleanedBlade[i];
                            new_factory.blade = blades[i];
                            new_factory.vocabulary = attributes[new_factory.clean];
                            factory[new_factory.clean] = new_factory;
                            
                            if (new_factory.clean.indexOf('@') >= 0) {
                                // The presence of @ mean it's calling a template
                                new_factory.template = new_factory.clean.replace('@', '');
                                string = string.replace(new_factory.blade, $('').getTemplate(new_factory.template, attributes));
                            } else if (new_factory.clean.indexOf('$') >= 0) {
                                var f = string.split('$');
                                new_factory.element = f[0];
                                new_factory.func = f[1];
                            } else {
                                if (typeof new_factory.vocabulary !== 'undefined') {
                                    string = string.replace(new_factory.blade, new_factory.vocabulary);
                                } else {
                                    string = string.replace(new_factory.blade, '');
                                }
                            }
                        }
                    }
                }                           
            } catch (error) { }
            return string;
        };
        
        var element = $(this);
        var dataOutput = element.attr('data-template-' + templateName);
        switch (templateName) {
            case 'charCountVocabulary':
                var defaultOutput = '<span class="cf-charCount-current">{{ current }}</span>&nbsp;/&nbsp;<span class="cf-charCount-total">{{ total }}</span>'
                break;
            case 'charCount':
                var defaultOutput = '<div class="cf-charCount"></div>';
                break;
            case 'selectWrapper':
                var defaultOutput = '<div id="{{ id }}" class="cf-select {{ classes }}">{{ @selectCurrentWrapper }}{{ @selectDropdownWrapper }}</div>';
                break;
            case 'selectCurrentWrapper':
                var defaultOutput = '<div class="cf-select-current">'
                defaultOutput += '<div class="cf-select-current-text">{{ current }}</div>'
                defaultOutput += '<div class="cf-select-current-arrow">&nbsp;</div>'
                defaultOutput += '</div>';
                break;
            case 'selectDropdownWrapper':
                var defaultOutput = '<div class="cf-select-drodown">'
                defaultOutput += '<ul>';
                defaultOutput += '{{ for (optionElement in optionsElements) }}';
                defaultOutput += '<li class="cf-select-option" data-value="{{ value }}">{{ text }}</li>';
                defaultOutput += '{{ endfor }}';
                defaultOutput += '</ul>';
                defaultOutput += '</div>';
                break;
            case 'checkradioWrapper':
                var defaultOutput = '<span id="{{ id }}" class="cf-{{ type }} {{ classes }} {{ value }}"></span>&nbsp;<span>{{ textSpan }}</span>';
                break;
            case 'submitWrapper':
                var defaultOutput = '<span id="{{ id }}" class="cf-submit {{ classes }}"><a href="#">{{ value }}</a></span>';
                break;
        }

        var output = dataOutput || jsOutput || defaultOutput;

        var factorizedOutput = factorize(output);

        return factorizedOutput;
    }
    
    var _structureForm = function(wrapper) {
    /* Take a form and structure it for standard */
        /* CORRECT FORM STRUCTURE
            <div class="element">
                <div class="label">{{ LABEL }}</div>
                <div class="input">{{ INPUT }}<div class="error"></div></div>
            </div>
        */
    
        var labels = wrapper.find('label');
        var isvalid = true;
        labels.each(function(_i, _e) {
            var label = $(_e);
            var target = $('[name="' + $(label).prop('for') + '"]')
            if( target.length === 0 ) target = $('#' + $(label).prop('for') );   
                     
            if (target.length === 0) {
                var target = $(label).find('input, textarea, select');
            }
            
            label.data('target', target);
            target.data('label', label);
            if (label.parents('.label').length < 1) {
                if( label.attr('data-option-skipLabel') !== "true" ) { // Not too sure of this option
                    console.error('The label ' + label.prop('for') + ' should be wrapped in a div with a class of label', label )
                    isvalid = false;
                }
            }

            if (target.parents('.input').length < 1) {
                console.error('The input ' + target.prop('name') + ' should be wrapped in a div with a class of input', target )
                isvalid = false;
            }
            
            if (label.parents('.element').length < 1) {
                console.error( 'The elements should be wrapped in a div with a class of element', label, target )
                isvalid = false;
            } 
        });
        
        if (isvalid) return true;
    };

    var _labelAsPlaceholder = function(index, element) {
    /* Take the label of an element and make it a placeholder */
        if (label.data('target')) {
            var label = elememt;
            var labelWrapper = label.parent('.label');
            var input = label.data('target');
            var inputWrapper = input.parent('.input');         
        }
    };    
    
    // Use aria-hidden on new elements
    
    $.fn.selectAsDivs = function( options ) {
        
    /* Take a select menu and make it a customizable div element */
            /* Take lots of information about the element */
        var element = $(this);
        var template, new_element, new_elementCurrent, new_elementDropdown;
        var id = element.attr('id') || element.attr('name') || element.data('name');
        var classes = element.attr('class');
        var current = element.find(':selected').text();
        var _o = element.children('option');
        var optionsElements = _o;

        /* Get a new dom element with the template helper */
        template = $(element.getTemplate('selectWrapper', { 'options': options, 'id': id, 'classes': classes, 'current': current, 'optionsElements': optionsElements }));
        /* Append the new dom element to the parent of the current element */
        element.parent().append(template);
        /* Add the new dom element to the data property of the element for later reference */
        element.data('selectWrapper', template);
        new_element = element.data('selectWrapper');
        new_element.prop('aria-hidden', 'true');
        element.data('selectCurrent', new_element.find('.cf-select-current'));
        new_elementCurrent = element.data('selectCurrent');
        element.data('selectDropdown', new_element.find('.cf-select-drodown'));
        new_elementDropdown = element.data('selectDropdown');
        /* Hide the element from view - but still accessible */
        /* element.css({ position: 'absolute', left: '-9999px' }); */
        /* Hide the dropdown, just for look */
        element.data('selectDropdown').hide();
        /* Bind lot of listener to the element for accessibility purposes */
        element.on( {
            'focus': function( event ) {
                if( !new_elementDropdown.is( ':visible' ) ) {
                    new_element.addClass( 'cf-select-focus' );
                    new_elementDropdown.slideDown( 'fast' );
                    
                    if( fc[id + 'FocusCallback'] ) {
                        fc[id + 'FocusCallback']( element );
                    }
                }
                
                var options = element.children( 'option' )
                var new_options = new_element.find( '.cf-select-option' )
                var optionCount = options.length - 1; // Minus one for good measure
                var currentOption = element.find( ':selected' );
                var currentOptionPosition = originalOptionPosition = currentOption.index();
                
                var selectCurrent = function( index ) {
                    selectedOption = new_options.eq( index );
                    new_options.removeClass( 'cf-select-option-active' );
                    selectedOption.addClass( 'cf-select-option-active' );
                    element.data( 'selectDropdown' ).scrollTop( element.data( 'selectDropdown' ).find( selectedOption ).position().top );
                }
                
                var revertToOriginalOption = function() {
                    selectCurrent( originalOptionPosition );
                }
                
                // When the element is on focus, we will activate keyboard controls for the options
                element.on( {
                    'keydown': function( event ) {
                        var keyCode = event.keyCode || event.which;
                        
                        if (keyCode == 38)  {
                            
                            if( currentOptionPosition === 0 ) {
                                currentOptionPosition = optionCount;
                            } else {
                                currentOptionPosition--;
                            }
                            selectCurrent( currentOptionPosition );
                            return false;
                        }
                    
                        if (keyCode == 40)  {                         
                            if( currentOptionPosition === optionCount ) {
                                currentOptionPosition = 0;
                            } else {
                                currentOptionPosition++;
                            }
                            selectCurrent( currentOptionPosition );
                            return false;
                        }
                        
                        if( ( keyCode === 13 ) || ( keyCode === 32 ) ) {
                            /* element.off( 'focus' ); */
                            element.trigger( 'change', element.children().eq( currentOptionPosition ) );
                            /* element.trigger( 'focus' ); */
                            element.off( 'blur', revertToOriginalOption );
                            return false;
                        }
                        
                    },
                    'blur': revertToOriginalOption()
                } );
            },
            'blur': function( event ) {
                if ( !options.killBlur ) {
                    if( new_elementDropdown.is( ':visible' ) ) {
                        new_element.removeClass( 'cf-select-focus' );
                        setTimeout( function( ) {
                            new_elementDropdown.slideUp( 'fast' );
                        }, 200 );
                        
                        if( fc[id + 'BlurCallback'] ) {
                            fc[id + 'BlurCallback']( element );
                        }
                    }
                }
                element.off('keydown');
            },
            'change': function( event, object ) {
                var toFocus = true;
                if ( !options.killBlur ) {
                    if( new_elementDropdown.is( ':visible' ) ) {
                        new_element.removeClass( 'cf-select-focus' );
                        setTimeout( function() {
                            new_elementDropdown.slideUp( 'fast' );
                        }, 200 );
                    }
                }
                if( typeof object != "undefined" ) {
                    try {
                        /* Check if it is a jQuery object */
                        if( $( object.target ).length == 0 ) {
                            throw "Not a jQuery Object";
                        }
                        
                        if( $( object.target ).hasClass( 'cf-select-option' ) ) {
                            var new_value = $( object.target ).data( 'value' );
                            var new_text = $( object.target ).text( );
                        } else {
                            return false;
                        }
                    } catch( errror ) {
                        var new_value = $( object ).data( 'value' ) || $( object ).val();
                        var new_text = $( object ).text( );
                        toFocus = false;
                    }
                } else {
                    try {
                        var new_value = $( event.target.selectedOptions[0] ).val( );
                        var new_text = $( event.target.selectedOptions[0] ).text( );
                    } catch( error ) {}
                }
                
                element.find('option[value="' + new_value + '"]').prop('selected', 'selected');

                new_elementCurrent.find('.cf-select-current-text').text(new_text);
                new_elementDropdown.find('.cf-select-option').removeClass('cf-select-option-active');
                new_elementDropdown.find('.cf-select-option[data-value="' + new_value + '"]').addClass('cf-select-option-active');

                if (fc[id + 'Callback']) {
                    fc[id + 'Callback'](element);
                }

                if( toFocus ) {
                    element.trigger( 'focus' );
                }
            }
        } );
        
        element.trigger( 'change', element.find(':selected') );
        
        new_element.on( { 
            'click': function( event ) {            
                if ( new_elementDropdown.is( ':visible' ) ) {
                    element.trigger( 'change', event );
                } else {
                    element.trigger( 'focus' );
                }
                
            } 
        } );
        
        b.customElement[id] = element;
    };

    $.fn.checkradioAsDivs = function( options ) {
    /* Take a radio or a checkbox and make it a customizable div element */
        var element = $( this );
        var template, new_element;
        var id = element.attr( 'id' ) || element.attr( 'name' );
        var type = element.is( ':checkbox' ) ? 'check' : 'radio';
        var value = element.val();
        var classes = element.attr( 'class' );
        var nextSpan = element.next( 'span' );
        if ( nextSpan.length > 0 ) {
            var textSpan = nextSpan.html();
            nextSpan.remove();
            template = $( element.getTemplate( 'checkradioWrapper', { 'options': options, 'id': id, 'type': type, 'classes': classes, 'value': value, 'textSpan': textSpan } ) );
        } else {
            template = $( element.getTemplate( 'checkradioWrapper', { 'options': options, 'id': id, 'type': type, 'classes': classes } ) );
        }
        element.data( type + 'Wrapper', template );
        new_element = element.data( type + 'Wrapper' );
        new_element.prop('aria-hidden', 'true');
        /* Append the new dom element to the parent of the current element */
        template.insertAfter( element );
        /* Hide the element from view - but still accessible */
        element.css( { position: 'absolute', left: '-9999px' } );
        
        element.on( {
            'focus': function( event ) {
                if( !new_element.hasClass( 'cf-checkradio-focus' ) ) {
                    new_element.addClass( 'cf-checkradio-focus ');
                    
                    if( fc[id + 'FocusCallback'] ) {
                        fc[id + 'FocusCallback']( element );
                    }
                }
            },
            'blur': function( event ) {
                if( new_element.hasClass( 'cf-checkradio-focus' ) ) {
                    new_element.removeClass( 'cf-checkradio-focus' );
                    
                    if( fc[id + 'BlurCallback'] ) {
                        fc[id + 'BlurCallback']( element );
                    }
                }
            },
            'change': function( event, object ) {
                element.trigger( 'focus' );

                if( type == 'radio' ) {
                    new_element.siblings( ).removeClass( 'ck-checkradio-checked' );
                    element.prop( 'checked', true );
                    new_element.addClass( 'ck-checkradio-checked' );
                } else {
                    try {
                        if( typeof object !== "undefined" ) {
                            if( object.target.tagName !== 'INPUT' ) {
                                if( element.is( ':checked' ) ) {
                                    element.prop( 'checked', false );
                                } else {
                                    element.prop( 'checked', true );
                                }
                            }
                        }
                    } catch( error ) {}
                    new_element.toggleClass('ck-checkradio-checked', element.is(':checked'));
                }
            }
        } );
                
        if( element.is(':checked') ) {
            element.trigger( 'change', element );
        }
        
        new_element.on( 'click', function( event ) {
            element.trigger( 'change', event );
        } );        
    };
    
    $.fn.submitAsDivs = function( options ) {
    /* Take a submit button and make it a customizable anchor element */
        var element = $( this );
        var template, new_element;
        var id = element.attr( 'id' ) || element.attr( 'name' );
        var classes = element.attr( 'class' );
        var value = element.val();
        
        template = $( element.getTemplate( 'submitWrapper', { 'options': options, 'id': id, 'classes': classes, 'value': value } ) );
        element.data( 'submitWrapper', template );
        new_element = element.data( 'submitWrapper' );
        new_element.prop('aria-hidden', 'true');
        /* Append the new dom element to the parent of the current element */
        template.insertAfter( element );
        /* Hide the element from view - but still accessible */
        element.css( { position: 'absolute', left: '-9999px' } );
        
        new_element.on( 'click', function( event ) {
            event.preventDefault();
            var form = element.parents('form');
            if( form.length > 0 ) {
                form.submit();
            }
        } );
    };
    
    $.fn.tabify = function() {
    /* Take a couple or more links and build a tab system from it */
        var tabWrapper = $( this );
        var tabHeadWrapper = tabWrapper.find( '.tab-head' );
        var tabBodyWrapper = tabWrapper.find( '.tab-body' );
        
        tabHeadWrapper.find( 'a' ).on( 'click', function(event) {
            event.preventDefault();
            var current = $( event.target );
            current.parent().siblings().removeClass( 'active' );
            current.parent().addClass( 'active' );

            var currentBody = $( '#' + current.data('tab') )
            tabBodyWrapper.addClass( 'hide' );
            currentBody.removeClass( 'hide' );
        } );
        
        var strippedHash = (window.location.hash).replace('#','');

        var hashedTabHeadWrapper = $('a[data-tab="' + strippedHash + '"]');
        if ( hashedTabHeadWrapper.length > 0 ) {
            hashedTabHeadWrapper.trigger( 'click' );
        } else {
            tabHeadWrapper.children().first().find( 'a' ).trigger( 'click' );
        }
    }

    $.fn.ajaxSubmit = function() {
    /* Take form and make the submission ajaxed */
    };

    // Make select multiple

    // CALBACK
    // FormID + InputName + Event + "Callback" 

    $.fn.makeFormAccessible = function( userOptions ) {
        var wrapper = $( this );
        var defaults = new Object;

        defaults.text = new Object;
        defaults.text = {
            'name': 'text',
            'validate': 'true',
            'charCount': 'false',
            'elements': wrapper.find( 'input[type="text"], input[type="email"], input[type="password"], input[type="file"], textarea' ),
            'labelAsPlaceholder': 'true',
            'label': 'true',
            'template': undefined,
            'callback': 'textCallback'
        };
        try {
            defaults.text = $.extend( defaults.text, userOptions.text );
        } catch( error ) {}

        
        defaults.select = new Object
        defaults.select = {
            'name': 'select',
            'customize': 'true',
            'structure': 'select',
            'validate': 'true',
            'elements': wrapper.find('select'),
            'label': 'true',
            'template': undefined,
            'callback': 'selectCallback'
        };
        try {
            defaults.select = $.extend( defaults.select, userOptions.select );
        } catch( error ) {}

        defaults.checkradio = new Object
        defaults.checkradio = {
            'name': 'checkradio',
            'customize': 'true',
            'structure': 'checkradio',
            'validate': 'true',
            'elements': wrapper.find( 'input[type="radio"], input[type="checkbox"]' ),
            'label': 'true',
            'template': undefined,
            'callback': 'checkradioCallback'
        };
        try {
            defaults.checkradio = $.extend( defaults.checkradio, userOptions.checkradio );
        } catch( error ) {}
        
        defaults.submit = new Object
        defaults.submit = {
            'name': 'submit',
            'customize': 'true',
            'structure': 'submit',
            'ajax': 'false',
            'validate': 'true',
            'elements': wrapper.find('input[type="submit"]', 'a.submit'),
            'template': undefined,
            'callback': 'submitCallback'
        };
        try {
            defaults.submit = $.extend( defaults.submit, userOptions.submit );
        } catch( error ) {}

        var options = defaults;
        
        wrapper.each(function( i, e ) {
            if (_structureForm( $( e ) )/*  true   */) {
                $.each( options, function() {
                    var objectName = this.name;
                    $.each( options[objectName].elements, function() {
                        var element = $(this);
                        var elementId = element.attr('id') || element.attr('name');
                        var optionLabel = (typeof element.attr('data-option-label') != 'undefined') ? element.attr('data-option-label') : options[objectName].label;
                        var optionLabelAsPlaceholder =  (typeof element.attr('data-option-labelAsPlaceholder') != 'undefined') ? element.attr('data-option-labelAsPlaceholder') : options[objectName].labelAsPlaceholder;
                        var optionValidate =  (typeof element.attr('data-option-validate') != 'undefined') ? element.attr('data-option-validate') : options[objectName].validate;
                        var optionCustomize =  (typeof element.attr('data-option-customize') != 'undefined') ? element.attr('data-option-customize') : options[objectName].customize;

                        if (optionLabel == 'false') {
                            /**
                             * OPTIONS
                             *
                             * toggle [data-option-label]
                             */
                             
                            element.data('label').parent().css( { position: 'absolute', left: '-9999px' } );
                            element.parent('.input').addClass('label-removed');
                        }
                        
                        if (optionLabelAsPlaceholder == 'true') {
                            /**
                             * OPTIONS
                             *
                             * toggle [data-option-labelAsPlaceholder]
                             */
                            try {
                                var labelValue = element.data('label').text();
                                element.prop('placeholder', labelValue);
                            } catch( error ) {}
                        }

                        if (optionCustomize == 'true') {
                            var template = element.getTemplate(objectName);
                            element.parent().append(template);
                            /* element.hide(); */
                        }
                        
                        if (objectName === 'text') {
                            // ALL THE TEXT GROUP INPUT SPECIFIC OPTIONS
                            var optionCharCount =  (typeof element.attr('data-option-charCount') != 'undefined') ? element.attr('data-option-charCount') : options.text.charCount;

                            if (optionCharCount == 'true') {
                                /**
                                 * OPTIONS
                                 *
                                 * toggle [data-option-labelAsPlaceholder]
                                 * template [data-template-charCount]
                                 * vocabulary [data-template-charCountVocabulary]
                                    ** {{ current }} , {{ total }}
                                 */
                                
                                var getCurrent = function() {
                                    charCountWrapper.html(element.getTemplate('charCountVocabulary', { 'options': options, 'current': element.val().length, 'total': element.prop('maxlength') } ));
                                }
                                
                                element.after(element.getTemplate('charCount'));
                                var charCountWrapper = element.next();
                                element.data('charCountWrapper', charCountWrapper);
                                element.on('keyup', getCurrent);
                                getCurrent();
                                
                            } /* End of optionCharCount */
                        } /* End of text options */
                        
                        if (objectName === 'select') {
                            // ALL THE SELECT GROUP INPUT SPECIFIC OPTIONS
                            if( optionCustomize === 'true' ) {
                                element.selectAsDivs(options.select);
                            }
                        } /* End of select options */
                        
                        if (objectName === 'checkradio') {
                            if( optionCustomize === 'true' ) {
                                element.checkradioAsDivs(options.checkradio);
                            }
                        } /* End of select checkradio */
                        
                        if (objectName === 'submit') {
                            var optionValidate = (typeof element.attr('data-option-validate') != 'undefined') ? element.attr('data-option-validate') : options[objectName].validate;
                            
                            if( optionCustomize === 'true' ) {
                                element.submitAsDivs(optionValidate);
                                
                                if( optionValidate ) {
                                    var form = element.parents('form');
                                    if( form.length > 0 ) {
                                        form.submit( function() {
                                            var results = form.validateForm();
                                            if( results ) {
                                                return true;
                                            } else {
                                                return false;
                                            }
                                        } );
                                    }
                                }
                            }
                            
                        } /* End of select options */
                        
                        if (document.createElement("input").placeholder == undefined) {
                            var placeholderValue = element.attr('placeholder');
                            
                            element.val(placeholderValue);
                            
                            element.on('focus', function() {
                                if ((element.val() === placeholderValue) || (element.val() === "")) {
                                    element.val('');
                                }
                            });
                            
                            element.on('blur', function() {
                                if (element.val() === "") {
                                    element.val(placeholderValue);
                                }
                            });
                        }
                                                
                        if (optionValidate === 'true') {
                            element.on('change', function() {
                                element.validate();
                            });
                        }
                                                
                        element.on('change', function() {
                            if (fc[objectName + 'Callback']) {
                                fc[objectName + 'Callback'](element);
                            }
                            if (fc[elementId + 'Callback']) {
                                fc[elementId + 'Callback'](element);
                            }
                        });
                        
                    }); /* End of each on options[objectName] */
                }); /* End of each on options */
            } /* End of structure flag */
        }); /* End of each on wrapper */
                
    }

    /* The function can be used with a form or a div element */
});